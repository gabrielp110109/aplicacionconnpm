Práctica| Mi primera aplicación con NPM
Vamos a crear nuestra primera aplicación con node.js utilizando dependencias.
1) Cree un directorio con nombre package_manager.
2) Entre al directorio y ejecute el comando para iniciar una aplicación de node con npm.
3) Instale las siguientes dependencias: Manejador de log's, herramienta de refresco en caliente, pruebas unitarias y lint.
4) Configure su proyecto con git correctamente.
5) Suba la liga de gitlab.

Gabriel Isai Prieto SAenz 353297

NPM (Node package manager)

NPM (Node package manager) es el gestor de dependencias para Node.js. Fue creado en el 2009 y es un proyecto de código abierto que ayuda a los desarrolladores javascript a manejar y compartir los árboles de dependencias de sus aplicaciones.
